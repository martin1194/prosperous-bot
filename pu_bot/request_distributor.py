class Request(BaseModel):
    """A single auction for an item."""

    msg_id: Optional[Snowflake] = None
    channel_id: Optional[Snowflake] = None
    guild_id: Snowflake
    creator_id: Snowflake
    id: str = Field(default_factory=lambda: str(uuid4()))
    item: str
    amount: int
    currency: str
    offer: list["Offer"] = []

    def to_map(self) -> dict[str, Any]:
        """Create a dictionary out of the data in this model."""
        return {
            "msg_id": self.msg_id,
            "channel_id": self.channel_id,
            "guild_id": self.guild_id,
            "creator_id": self.creator_id,
            "id": self.id,
            "item": self.item,
            "amount": self.amount,
            "currency": self.currency,
            "offer": [
                {
                    "user_id": i.user_id,
                    "username": i.username,
                    "offer_id": i.offer_id,
                    "price": i.price,
                    "max_amount": i.max_amount,
                    "location": i.location,
                }
                for i in self.bids
            ],
        }
