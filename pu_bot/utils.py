"""General purpose utilities for use throughout the bot."""

from hikari import Snowflake
from .bot import bot


async def get_username(guild_id: Snowflake, user_id: Snowflake) -> str:
    """Get the username for a user."""
    user = await bot.rest.fetch_member(guild_id, user_id)
    return user.display_name
