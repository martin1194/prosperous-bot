""" Register commands and views for the bot. """

import logging
import hikari
import lightbulb
import miru
#from multiprocessing import Value
#from hikari import Snowflake, components
from hikari import Snowflake


from pu_bot.auction_distributor import (
    AUCTIONS,
    Auction,
    DuplicateBid,
    complete_auction,
    create_auction,
    create_bid,
    remove_bid,
    save_auctions,
)
from pu_bot.utils import get_username

from .bot import bot

LOGGER = logging.getLogger(__name__)


class BidButton(miru.Button):
    """The button that triggers new bids to be added to the auction."""

    view: "AuctionView"

    def __init__(self, auction: Auction):
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Bid!",
            custom_id="bid_button_" + auction.id,
        )
        self.auction = auction

    async def callback(self, context: miru.ViewContext) -> None:
        """Create a new bid and update the message."""
        assert context.guild_id is not None
        username = await get_username(context.guild_id, context.user.id)
        try:
            create_bid(context.user.id, username, self.auction.id)
        except (
            DuplicateBid
        ):  # If user already registered as bidder, remove them from the auction
            remove_bid(context.user.id, self.auction.id)
        except ValueError as error:
            await context.respond(
                content=str(error), flags=hikari.MessageFlag.EPHEMERAL
            )
            return

        await self.view.update_active_message()


class FinishButton(miru.Button):
    """Button that allows for completing an auction and calculating the results."""

    view: "AuctionView"

    def __init__(self, auction: Auction):
        super().__init__(
            style=hikari.ButtonStyle.SUCCESS,
            label="Finish",
            custom_id="finish_button_" + auction.id,
        )
        self.auction = auction

    async def callback(self, context: miru.ViewContext) -> None:
        """Finish out the auction attached to this view."""
        if context.user.id != self.auction.creator_id:
            await context.respond(
                content="Only the creator of an auction can finish it",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        await self.view.complete_auction()


class ChangeAmountButton(miru.Button):
    """Allows updating the maximum amount a bidder would like to receive from an auction."""

    view: "AuctionView"

    def __init__(self, auction: Auction):
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Change Bid Amount",
            custom_id="change_bid_amount_button_" + auction.id,
        )
        self.auction = auction

    async def callback(self, context: miru.ViewContext) -> None:
        # Find the bid with the user_id that matches context.user.id
        foundID = False
        for bid in self.auction.bids:
            if bid.user_id == context.user.id:
                modal = ChangeBidAmountModal(
                    auction=self.auction, user_id=bid.user_id, view=self.view
                )
                await context.respond_with_modal(modal)
                foundID = True
        if not foundID:
            await context.respond(
                content="Please bid before you set a max amount",
                flags=hikari.MessageFlag.EPHEMERAL,
            )


class ChangeBidAmountModal(miru.Modal):
    def __init__(self, auction: Auction, user_id: Snowflake, view: "AuctionView"):
        super().__init__(
            title="Change Max Bid Amount",
            custom_id="ChangeBidAmountModal" + auction.id,
        )
        self.auction = auction
        self.user_id = user_id
        self.view = view

        self.bid = None
        for bid in auction.bids:
            if bid.user_id == user_id:
                self.bid = bid

        assert (
            self.bid is not None
        ), f"Unable to locate bid on auction {auction.id} from user {user_id}"

        # Set placeholder to the amount the max
        self.max_amount.placeholder = str(self.bid.max_amount or auction.amount)

    max_amount = miru.TextInput(
        label="Max Amount",
        placeholder=("Max amount you'd like to receive"),
        required=True,
    )

    # The callback function is called after the user hits 'Submit'
    async def callback(self, ctx: miru.ModalContext) -> None:
        if self.max_amount.value is not None and self.max_amount.value.isdigit():
            for bid in self.auction.bids:
                if bid.user_id == self.user_id:
                    bid.max_amount = int(self.max_amount.value)
            await self.view.update_active_message()
            await ctx.respond(
                content=(
                    f"Bid request maximum amount changed to {self.max_amount.value}"
                ),
                flags=hikari.MessageFlag.EPHEMERAL,
            )
        else:
            await ctx.respond(
                content=(f"Please enter number."),
                flags=hikari.MessageFlag.EPHEMERAL,
            )
        # You can also access the values using ctx.values, Modal.values, or use ctx.get_value_by_id()


class AuctionView(miru.View):
    """A view for the context."""

    def __init__(self, auction: Auction, *args, **kwargs) -> None:
        kwargs["timeout"] = None
        super().__init__(*args, **kwargs)

        self.auction = auction
        self.add_item(BidButton(auction))
        self.add_item(FinishButton(auction))
        self.add_item(ChangeAmountButton(auction))

    async def update_active_message(self, message_owned: bool = True) -> str:
        """Update the message on the view to contain most recent information."""
        new_message = (
            f"{self.auction.amount} {self.auction.item}@{self.auction.price:.2f} "
            f"{self.auction.currency}/u available on {self.auction.location}"
        )
        if len(self.auction.bids) > 0:
            new_message += "\nBids:"
        for bid in self.auction.bids:
            max_amount = bid.max_amount or bid.auction.amount
            new_message += f"\n* {bid.username}: Up to {max_amount}"

        if message_owned:
            assert self.message is not None
            await self.message.edit(content=new_message)
            self.message.content = new_message

        return new_message

    async def complete_auction(self) -> None:
        """Close out the auction and display the results."""
        message = await complete_auction(self.auction)
        assert self.message is not None
        await self.message.edit(content=message, components=[])
        self.stop()

    async def on_timeout(self) -> None:
        """Close out the auction after a period of inactivity"""

        await self.complete_auction()


@bot.listen()
async def startup_views(event: hikari.StartedEvent) -> None:
    """Load auctions from persistence on startup."""
    for auction in AUCTIONS.values():
        view = AuctionView(auction)
        if auction.channel_id is None or auction.msg_id is None:
            LOGGER.warning(
                f"Unable to reload {auction} due to missing channel and/or message ids"
            )
            continue
        message = await event.app.rest.fetch_message(auction.channel_id, auction.msg_id)
        await view.start(message)


@bot.command
@lightbulb.command("ping", "Check if the bot is alive")
@lightbulb.implements(lightbulb.SlashCommand)
async def ping(ctx: lightbulb.Context) -> None:
    """Simple ping handler to test bot functionality"""

    await ctx.respond("Pong!")


@bot.command
@lightbulb.option(
    name="item",
    type=str,
    description="The item you want to create an auction for",
    required=True,
)
@lightbulb.option(
    name="amount",
    type=int,
    description="The amount of items you want to auction off",
    required=True,
)
@lightbulb.option(
    name="price",
    type=float,
    description="The price of each item you want to auction off",
    required=True,
)
@lightbulb.option(
    name="location",
    type=str,
    description="The location of the auction.",
    required=True,
)
@lightbulb.option(
    name="currency",
    type=str,
    description="The currency to use for the auction.",
    default="AIC",
    required=False,
)
@lightbulb.command("auction", "Start a new auction")
@lightbulb.implements(lightbulb.SlashCommand)
async def create_new_auction(ctx: lightbulb.Context) -> None:
    """Create a new auction for an item."""

    assert (
        ctx.guild_id is not None
    ), "Auctions should only be created in the context of a guild"

    auction = create_auction(
        guild_id=ctx.guild_id,
        user_id=ctx.user.id,
        item=ctx.options.item,
        amount=ctx.options.amount,
        price=ctx.options.price,
        location=ctx.options.location,
        currency=ctx.options.currency,
    )

    view = AuctionView(auction)

    message = await ctx.respond(
        await view.update_active_message(False),
        components=view,
    )

    msg = await message.message()
    auction.msg_id = msg.id
    auction.channel_id = msg.channel_id
    save_auctions()

    await view.start(message)
    await view.wait()


def create_message_link(
    guild_id: Snowflake, channel_id: Snowflake, msg_id: Snowflake
) -> str:
    return f"https://discord.com/channels/{guild_id}/{channel_id}/{msg_id}"


@bot.command
@lightbulb.command("list_auctions", "List all active auctions in the current server")
@lightbulb.implements(lightbulb.SlashCommand)
async def list_all_active_auction(ctx: lightbulb.Context) -> None:
    """List all available auctions"""
    assert (
        ctx.guild_id is not None
    ), "Auctions should only be listed in the context of a guild"

    response = []
    for auction in AUCTIONS.values():
        if auction.guild_id != ctx.guild_id:
            continue

        link = create_message_link(auction.guild_id, auction.channel_id, auction.msg_id)
        response.append(
            f"{auction.amount} {auction.item}@{auction.price:.2f} "
            f"{auction.currency}/u available on {auction.location}: {link}"
        )

    if not response:
        await ctx.respond("No active auctions.")
        return

    await ctx.respond("\n".join(response))
