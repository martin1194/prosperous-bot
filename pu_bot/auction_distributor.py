"""Track auctions that allow equal distribution and sale of materials."""

import json
import os

from typing import Any, Optional
from uuid import uuid4
from hikari import Snowflake
from pydantic import BaseModel, Field, validate_arguments

from pu_bot.utils import get_username
from pu_bot.bot import SETTINGS

# TODO: Persist this with a database (or at least a file) later
AUCTIONS: dict[str, "Auction"] = {}


class DuplicateBid(Exception):
    """Raised when a duplicate bid is attempted"""


class Auction(BaseModel):
    """A single auction for an item."""

    msg_id: Optional[Snowflake] = None
    channel_id: Optional[Snowflake] = None
    guild_id: Snowflake
    creator_id: Snowflake
    id: str = Field(default_factory=lambda: str(uuid4()))
    item: str
    amount: int
    price: float
    location: str
    currency: str
    bids: list["Bid"] = []

    def to_map(self) -> dict[str, Any]:
        """Create a dictionary out of the data in this model."""
        return {
            "msg_id": self.msg_id,
            "channel_id": self.channel_id,
            "guild_id": self.guild_id,
            "creator_id": self.creator_id,
            "id": self.id,
            "item": self.item,
            "amount": self.amount,
            "price": self.price,
            "location": self.location,
            "currency": self.currency,
            "bids": [
                {
                    "user_id": i.user_id,
                    "username": i.username,
                    "auction_id": i.auction_id,
                    "max_amount": i.max_amount,
                }
                for i in self.bids
            ],
        }


class Bid(BaseModel):
    """A bid on an auction"""

    user_id: Snowflake
    username: str
    auction_id: str
    max_amount: Optional[int]

    @property
    def auction(self) -> Auction:
        """Get the auction for this bid."""
        return AUCTIONS[self.auction_id]


AuctionResult = dict[Snowflake, int]


def save_auctions() -> None:
    """Store off the current auctions map"""
    save_map = {}
    for auction_id, auction in AUCTIONS.items():
        save_map[auction_id] = auction.to_map()
    with open(SETTINGS.savefile, "w", encoding="utf-8") as savefile:
        savefile.write(json.dumps(save_map))


def load_auctions():
    """Load any active auctions from the save file"""
    auctions = {}
    if not os.path.exists(SETTINGS.savefile):
        file = open(SETTINGS.savefile, "w")
        file.close()

    with open(SETTINGS.savefile, "r", encoding="utf-8") as savefile:
        json_str = savefile.read()
        if len(json_str) > 1:
            auctions = json.loads(json_str)

    for auction_id in auctions:
        auction = auctions[auction_id]
        print("Loading auction ", auction)
        AUCTIONS[auction_id] = Auction(
            msg_id=auction["msg_id"],
            channel_id=auction["channel_id"],
            guild_id=auction["guild_id"],
            creator_id=auction["creator_id"],
            id=auction["id"],
            item=auction["item"],
            amount=auction["amount"],
            price=auction["price"],
            location=auction["location"],
            currency=auction["currency"],
        )
        for bid in auction["bids"]:
            AUCTIONS[auction_id].bids.append(
                Bid(
                    user_id=bid["user_id"],
                    username=bid["username"],
                    auction_id=bid["auction_id"],
                    max_amount=bid["max_amount"],
                )
            )


@validate_arguments
def create_auction(  # pylint: disable=too-many-arguments
    guild_id: Snowflake,
    user_id: Snowflake,
    item: str,
    amount: int,
    price: float,
    location: str,
    currency: str,
) -> Auction:
    """Start an auction"""

    auction = Auction(
        guild_id=guild_id,
        creator_id=user_id,
        item=item,
        amount=amount,
        price=price,
        location=location,
        currency=currency,
    )

    assert auction.id not in AUCTIONS
    AUCTIONS[auction.id] = auction
    save_auctions()

    return auction


@validate_arguments
def create_bid(
    user_id: Snowflake, username: str, auction_id: str, max_amount: Optional[int] = None
) -> Bid:
    """Create a new bid for an auction"""

    if auction_id not in AUCTIONS:
        raise ValueError(f"Auction {auction_id} does not exist")

    auction = AUCTIONS[auction_id]

    for bid in auction.bids:
        if user_id == bid.user_id:
            raise DuplicateBid("You can only bid on an auction once")

    bid = Bid(
        user_id=user_id, username=username, auction_id=auction_id, max_amount=max_amount
    )

    bid.auction.bids.append(bid)
    save_auctions()

    return bid


@validate_arguments
def remove_bid(user_id: Snowflake, auction_id: str) -> None:
    """Remove a user from an auction"""

    if auction_id not in AUCTIONS:
        raise ValueError(f"Auction {auction_id} does not exist")

    auction = AUCTIONS[auction_id]

    bids = []
    for bid in auction.bids:
        if user_id != bid.user_id:
            bids.append(bid)

    auction.bids = bids

    save_auctions()


def calculate_auction_results(user_id: Snowflake, auction_id: str) -> AuctionResult:
    """Finish an auction and calculate distribution of items to the bidders."""

    if auction_id not in AUCTIONS:
        raise ValueError(f"Auction {auction_id} does not exist")

    auction: Auction = AUCTIONS[auction_id]

    if user_id != auction.creator_id:
        raise ValueError("Only the creator of an auction can finish it")

    if not auction.bids:
        return {}

    for bid in auction.bids:
        if bid.max_amount is None:
            bid.max_amount = auction.amount

    auction_results: AuctionResult = {bid.user_id: 0 for bid in auction.bids}

    items_available = auction.amount
    resolved = False

    # Assign everyone the highest equal share, save remaining items
    while not resolved:
        max_amount_per = items_available // (len(auction.bids) or 1)
        if max_amount_per == 0:
            break
        for bid in list(auction.bids):
            assert bid.max_amount is not None
            currently_given = auction_results[bid.user_id]

            amount_to_give = min(bid.max_amount - currently_given, max_amount_per)

            auction_results[bid.user_id] += amount_to_give
            items_available -= amount_to_give

            if auction_results[bid.user_id] >= bid.max_amount:
                auction.bids.remove(bid)

            if items_available == 0 or len(auction.bids) == 0:
                resolved = True
                break

    return auction_results


async def complete_auction(auction: Auction) -> str:
    """Finish out an auction."""

    results = calculate_auction_results(
        user_id=auction.creator_id, auction_id=auction.id
    )

    message = [
        (
            f"Auction on {auction.location} for {auction.amount} {auction.item}@{auction.price} "
            f"{auction.currency}/u results:"
        )
    ]
    for user_id, amount in results.items():
        user_name: str = await get_username(auction.guild_id, user_id)

        message.append(
            f"{user_name}: {amount} {auction.item} "
            f"for a total of {amount*auction.price} {auction.currency}"
        )

    result = "\n* ".join(message)

    unallocated_items = auction.amount - sum(results.values())
    del AUCTIONS[auction.id]
    save_auctions()
    return f"{result}\n\nUnallocated items: {unallocated_items} {auction.item}"
