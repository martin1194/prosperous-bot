""""Test module for the auction_distributor.py module"""

from unittest.mock import MagicMock, patch

import pytest
from hikari import Snowflake

from pu_bot.auction_distributor import AUCTIONS, Auction, Bid, calculate_auction_results


@pytest.mark.parametrize(
    "auction_amount, bids, expected_results",
    [
        (10000, [2000, 2000, 10000], [2000, 2000, 6000]),
        (800, [200, 800, 800], [200, 300, 300]),
        (125, [50, 125, 40], [42, 42, 40]),
        (610, [100], [100]),
        (6900, [6900, 6900, 300, 6900, 6900], [1650, 1650, 300, 1650, 1650]),
        (150, [150, 150], [75, 75]),
        (70, [25], [25]),
        (250, [25], [25]),
        (1000, [1000, 200, 1000], [400, 200, 400]),
        (3405, [3405, 3405, 3405, 3405], [851, 851, 851, 851]),
        (300, [300, 300, 300, 300], [75, 75, 75, 75]),
        (300, [], []),
    ],
)
def test_auction_results(auction_amount, bids, expected_results):
    # Set up auction
    auction = Auction(
        msg_id=None,
        channel_id=None,
        guild_id=MagicMock(spec=Snowflake),
        creator_id=MagicMock(spec=Snowflake),
        item="Test",
        amount=auction_amount,
        price=1,
        location="TST",
        currency="tst",
    )

    auction_bids = [
        Bid(user_id=i, username=f"tester-{i}", auction_id=auction.id, max_amount=amount)
        for i, amount in enumerate(bids)
    ]

    auction.bids = auction_bids

    with patch.dict(AUCTIONS, {auction.id: auction}):
        results = calculate_auction_results(auction.creator_id, auction.id)

    assert list(results.values()) == expected_results
